class CreateCsvs < ActiveRecord::Migration[6.1]
  def change
    create_table :csvs do |t|
      t.string :title
      t.text :description
      t.string :slug
      t.text :csv_data,  limit: 65535

      t.timestamps
    end
  end
end
