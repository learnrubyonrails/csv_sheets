class AddColumnToCsv < ActiveRecord::Migration[6.1]
  def change
    add_column :csvs, :csv_file, :string
  end
end
