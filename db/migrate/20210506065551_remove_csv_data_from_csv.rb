class RemoveCsvDataFromCsv < ActiveRecord::Migration[6.1]
  def change
    remove_column :csvs, :csv_data, :text
  end
end
