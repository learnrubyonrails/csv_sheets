class AddDataColumnToCsv < ActiveRecord::Migration[6.1]
  def change
    add_column :csvs, :csv_data, :string, array: true, default: []
  end
end
