const { environment } = require('@rails/webpacker')
const jquery = require('./plugins/jquery')
const webpack = require('webpack')
environment.plugins.append('Provider',
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        Propper: ['proper.js','default']
    }))
environment.plugins.prepend('jquery', jquery)
module.exports = environment
