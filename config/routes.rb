Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'csvs#new'
  resources :csvs

  require 'sidekiq/web'
  # or require 'sidekiq/pro/web'
  # or require 'sidekiq-ent/web'

  CsvSheets::Application.routes.draw do
    mount Sidekiq::Web => "/sidekiq" # mount Sidekiq::Web in your Rails app
  end
end
