class CsvsController < ApplicationController

    before_action :authenticate_user!
    # before_action :set_csv_params ,only: [:create]
    def index
        @csv_data = Csv.all
    end

    def new
        @csv = Csv.new
    end

    def edit

    end

    def create
        @csv = Csv.new(set_csv_params)
         if @csv.save
            CsvWorker.perform_async(@csv.csv_file.file.path,@csv.id)
            flash["notice"] = "Uploading the File"
            redirect_to csvs_path
        else
            render  'new'
        end
    end

    def update
    end


    def destory
    end

    private
        def set_csv_params
            params.require(:csv).permit(:title,:description,:csv_file)
        end
end
