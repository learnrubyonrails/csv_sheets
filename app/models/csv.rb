class Csv < ApplicationRecord
   mount_uploader :csv_file, CsvFileUploader
   before_create :set_slug

   validates :title,:description,:csv_file ,presence: true
   validates  :title, uniqueness: true
   validates :title , length: {minimum: 5}

   def set_slug
     self.slug = title.parameterize
   end
end
