class CsvWorker
  include Sidekiq::Worker
  require 'csv'

  def perform(csv,id)
    csv_sheet_data = []
    CSV.foreach(csv).each do |row|
      puts row.inspect
      csv_sheet_data << row.inspect
    end
    if csv_sheet_data.present?
      Csv.find(id)&.update(csv_data:csv_sheet_data)
    end
  end
end
